﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using XBoxOneCarController.Code;

namespace XBoxOneCarController.Api
{
    class ApiService : IApiService
    {
        private HttpClient _client = new HttpClient { Timeout = TimeSpan.FromSeconds(2) };

        public string Uri { get; }

        public ApiService(string uri)
            => Uri = uri;

        public async Task Stop()
        {
            Console.WriteLine($"{Uri}motor/stop");

            SendGetAndCatchExceptions($"{Uri}motor/stop");
        }

        public async Task GoForward(int value)
        {
            Console.WriteLine($"{Uri}motor/forward/{value}");

            if (!value.IsBetween(0, 90))
                Console.WriteLine($"INVALID GoForward value: {value}");
            else
                SendGetAndCatchExceptions($"{Uri}motor/forward/{value}");
        }

        public async Task GoBackward(int value)
        {
            Console.WriteLine($"{Uri}motor/backward/{value}");

            if (!value.IsBetween(0, 90))
                Console.WriteLine($"INVALID GoBackward value: {value}");
            else
                SendGetAndCatchExceptions($"{Uri}motor/backward/{value}");
        }

        public async Task TurnStraight()
        {
            Console.WriteLine($"{Uri}servo/90 (straight)");

            SendGetAndCatchExceptions($"{Uri}servo/90");
        }

        public async Task TurnLeft(int value)
        {
            Console.WriteLine($"{Uri}servo/{value} (left)");

            if (!value.IsBetween(30, 90))
                Console.WriteLine($"INVALID TurnLeft value: {value}");
            else
                SendGetAndCatchExceptions($"{Uri}servo/{value}");
        }

        public async Task TurnRight(int value)
        {
            Console.WriteLine($"{Uri}servo/{value} (right)");

            if (!value.IsBetween(90, 150))
                Console.WriteLine($"INVALID TurnRight value: {value}");
            else
                SendGetAndCatchExceptions($"{Uri}servo/{value}");
        }
        
        private async void SendGetAndCatchExceptions(string uri)
        {
            var message = new HttpRequestMessage(HttpMethod.Get, uri);

            try
            {
                await _client.SendAsync(message);
            }
            catch (Exception ex)
                when (ex is ArgumentNullException
                    || ex is InvalidOperationException
                    || ex is HttpRequestException
                    || ex is TaskCanceledException)
            {
                Console.WriteLine($"!!! ERROR : {ex.GetType().Name} : {ex.Message}");
            }
        }
    }
}
