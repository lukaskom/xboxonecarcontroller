﻿using System.Threading.Tasks;

namespace XBoxOneCarController.Api
{
    interface IApiService
    {
        Task Stop();
        Task GoForward(int value);
        Task GoBackward(int value);

        Task TurnStraight();
        Task TurnLeft(int value);
        Task TurnRight(int value);
    }
}
