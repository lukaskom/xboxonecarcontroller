﻿using System;
using System.Threading.Tasks;

namespace XBoxOneCarController.Api
{
    class PrintToConsole_ApiService : IApiService
    {
        public async Task Stop()
            => Console.WriteLine($"motor/stop");

        public async Task GoBackward(int value)
            => Console.WriteLine($"motor/backward/{value}");

        public async Task GoForward(int value)
            => Console.WriteLine($"motor/forward/{value}");

        public async Task TurnLeft(int value)
            => Console.WriteLine($"servo/{value} (left)");

        public async Task TurnRight(int value)
            => Console.WriteLine($"servo/{value} (right)");

        public async Task TurnStraight()
            => Console.WriteLine($"servo/90 (straight)");
    }
}
