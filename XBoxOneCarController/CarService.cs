﻿using System;
using System.Threading.Tasks;
using XBoxOneCarController.Code;
using XBoxOneCarController.Api;

namespace XBoxOneCarController
{
    class CarService
    {
        private IApiService _apiService;

        public CarService(IApiService apiService)
            => _apiService = apiService;

        public int RightTrigger { get; private set; } = 0;
        public int LeftTrigger { get; private set; } = 0;
        public Point LeftThumb { get; private set; } = new Point(0, 0);
        public int RightOrLeftValue => LeftThumb.X;
        public Point RightThumb { get; private set; } = new Point(0, 0);

        public async Task UpdateController(ControllerChangedArgs args)
        {
            //INFO: B serves as a panic button
            if (args.ButtonB_Pressed)
            {
                await _apiService.Stop();
                await _apiService.TurnStraight();

                return;
            }

            await TurnRightOrLeft(args.LeftThumb.X);
            await MoveForwardOrBackward(args.LeftTrigger, args.RightTrigger);

            UpdateValues(args);
        }

        private void UpdateValues(ControllerChangedArgs args)
        {
            LeftTrigger = args.LeftTrigger;
            RightTrigger = args.RightTrigger;
            LeftThumb = args.LeftThumb.HardCopy();
            RightThumb = args.RightThumb.HardCopy();
        }

        private async Task TurnRightOrLeft(int rightOrLeftValue)
        {
            if (AreValuesSimilar(RightOrLeftValue, rightOrLeftValue, 10))
                return;

            // Turh wheels straight
            if (Math.Abs(rightOrLeftValue) <= 10)
                await _apiService.TurnStraight();

            // Turn wheels to the right
            else if (rightOrLeftValue > 0)
                await _apiService.TurnRight(90 + (rightOrLeftValue * 60 / 100));

            // Turn wheels to the left
            else
                await _apiService.TurnLeft(90 + (rightOrLeftValue * 60 / 100));
        }

        private async Task MoveForwardOrBackward(int leftTrigger, int rightTrigger)
        {
            if (AreValuesSimilar(LeftTrigger, leftTrigger, 10)
                    && AreValuesSimilar(RightTrigger, rightTrigger, 10))
                return;

            // Stop vehicle
            if (leftTrigger < 10 && rightTrigger < 10)
                await _apiService.Stop();

            // Move forward
            else if (rightTrigger >= 10)
                await _apiService.GoForward(rightTrigger * 90 / 255);

            // Move backward
            else
                await _apiService.GoBackward(leftTrigger * 90 / 255);
        }

        private bool AreValuesSimilar(int a, int b, int range)
            => Math.Abs(a - b) <= range;
    }
}
