﻿namespace XBoxOneCarController.Code
{
    class ControllerChangedArgs
    {
        /// <summary>
        /// Value between 0 and 255.
        /// </summary>
        public int RightTrigger { get; set; }

        /// <summary>
        /// Value between 0 and 255.
        /// </summary>
        public int LeftTrigger { get; set; }

        /// <summary>
        /// Values between -100 and 100.
        /// </summary>
        public Point LeftThumb { get; set; }

        /// <summary>
        /// Values between -100 and 100.
        /// </summary>
        public Point RightThumb { get; set; }

        public bool ButtonB_Pressed { get; set; }
    }
}
