﻿namespace XBoxOneCarController.Code
{
    static class Extensions
    {
        public static bool IsBetween(this int value, int min, int max)
            => value >= min && value <= max;
    }
}
