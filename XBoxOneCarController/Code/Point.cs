﻿namespace XBoxOneCarController.Code
{
    public class Point
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }

        public Point HardCopy()
            => new Point(X, Y);
    }
}