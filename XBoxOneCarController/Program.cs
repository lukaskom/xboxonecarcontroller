﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using XBoxOneCarController.Code;
using XBoxOneCarController.Api;

namespace XBoxOneCarController
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var controller = new XInputController();
            if (!controller.IsConnected)
            {
                Console.WriteLine("Gamepad not connected.");
                Console.ReadLine();
                return;
            }

            var apiService = new ApiService("http://192.168.1.170/");
            var carController = new CarService(apiService);

            while (true)
            {
                try
                {
                    await Task.Delay(250);
                    controller.Update();

                    var controllerArgs = new ControllerChangedArgs
                    {
                        LeftThumb = controller.LeftThumb.HardCopy(),
                        RightThumb = controller.RightThumb.HardCopy(),
                        LeftTrigger = (int)controller.LeftTrigger,
                        RightTrigger = (int)controller.RightTrigger,
                        ButtonB_Pressed = controller.ButtonB_Pressed
                    };

                    await carController.UpdateController(controllerArgs);
                }
                catch (SharpDX.SharpDXException ex)
                {
                    Console.WriteLine($"!!! ERROR : {ex.GetType().Name} : {ex.Message}");
                    continue;
                }
                
                Debug.WriteLine(
                    $"left.X {controller.LeftThumb.X.ToString("000")} ; \n" +
                    $"left.Y {controller.LeftThumb.Y.ToString("000")} ; \n" +
                    $"right.X {controller.RightThumb.X.ToString("000")} ; \n" +
                    $"right.Y {controller.RightThumb.Y.ToString("000")} ; \n" +
                    $"leftTrigger {controller.LeftTrigger}\n" +
                    $"rightTrigger {controller.RightTrigger}\n");
            }
        }
    }
}
