﻿using SharpDX.XInput;
using System;
using XBoxOneCarController.Code;

namespace XBoxOneCarController
{
    class XInputController
    {
        Controller controller;
        Gamepad gamepad;
        public bool IsConnected { get; } = false;
        public int Deadband { get; } = 2500;
        public Point LeftThumb { get; } = new Point(0, 0);
        public Point RightThumb { get; } = new Point(0, 0);
        public float LeftTrigger { get; private set; }
        public float RightTrigger { get; private set; }
        public bool ButtonB_Pressed { get; set; }

        public XInputController()
        {
            controller = new Controller(UserIndex.One);
            IsConnected = controller.IsConnected;
        }

        // Call this method to update all class values
        public void Update()
        {
            if (!IsConnected)
                return;

            gamepad = controller.GetState().Gamepad;

            var leftThumbX = (int)((Math.Abs((float)gamepad.LeftThumbX) < Deadband) ? 0 : (float)gamepad.LeftThumbX / short.MinValue * -100);
            var leftThumbY = (int)((Math.Abs((float)gamepad.LeftThumbY) < Deadband) ? 0 : (float)gamepad.LeftThumbY / short.MaxValue * 100);
            var rightThumbY = (int)((Math.Abs((float)gamepad.RightThumbX) < Deadband) ? 0 : (float)gamepad.RightThumbX / short.MaxValue * 100);
            var rightThumbX = (int)((Math.Abs((float)gamepad.RightThumbY) < Deadband) ? 0 : (float)gamepad.RightThumbY / short.MaxValue * 100);

            LeftThumb.X = leftThumbX;
            LeftThumb.Y = leftThumbY;
            RightThumb.Y = rightThumbY;
            RightThumb.X = rightThumbX;

            LeftTrigger = gamepad.LeftTrigger;
            RightTrigger = gamepad.RightTrigger;

            ButtonB_Pressed = ((gamepad.Buttons & GamepadButtonFlags.B) == GamepadButtonFlags.B);
        }
    }
}
